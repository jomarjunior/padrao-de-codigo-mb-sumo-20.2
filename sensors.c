/* MINERVABOTS
 * 2020.2
 * <Descrição do arquivo>
 * Esse arquivo tem como intuito definir um modelo para a programação do novo código dos robôs de sumô da equipe.
 * As lógicas e os métodos de programação apresentados aqui servem apenas de modelos e/ou exemplos sem qualquer
 * relação com a realidade. {Substitui <Descrição do arquivo>}
 */

#include "sensors.h"
#include "types.h"

/* uint8_t GetDigitalSensorPin ( sensorType sensor )
 * Retorna o valor do pino associado ao sensor digital fornecido em 'sensor'.
 * sensorType sensor (I) - Inteiro que representa um sensor digital */
uint8_t
GetDigitalSensorPin ( sensorType sensor)
{
    return DIGITAL_SENSOR_PINOUT[sensor];
}

/* bool GetDigitalSensorReadings ( sensorType )
 * Retorna o valor de leitura do sensor associado ao inteiro fornecido em 'sensor'
 * sensorType sensor (I) - Inteiro que representa um sensor digital */
bool
GetDigitalSensorReadings ( sensorType sensor )
{
    return digitalRead ( GetDigitalSensorPin (sensor) );
}