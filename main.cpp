/* MINERVABOTS
 * A documentação relacionada a este código encontra-se na nossa wiki.
 * Wiki: <link-da-wiki>
 * <Descrição do arquivo>
 * Esse arquivo tem como intuito definir um modelo para a programação do novo código dos robôs de sumô da equipe.
 * As lógicas e os métodos de programação apresentados aqui servem apenas de modelos e/ou exemplos sem qualquer
 * relação com a realidade. {Substitui <Descrição do arquivo>}
 */


/* Inclusão de bibliotecas do sistema */
#include <Arduino.h>

/* Incluir as bibliotecas que definem a pinagem para cada robô */
#ifdef __SUMO_3KG__
    /* Robôs de 3kg */
    #ifdef __ATENA__
        /* Para a atena */
        #include "AtenaPinoutReference.h"
    #endif
    #ifndef __ATENA__
        /* Outros robôs de 3Kg que não a Atena */
        #include "3KgPinoutReference.h"
    #endif
#endif

#ifdef __MINI_SUMO__
    /* Para os robôs de mini sumô */
    #include "miniPinoutReference.h"
#endif

/* Inclusão de bibliotecas gerais */
#include "sensors.h"
#include "types.h"

#define MACRO_CONSTANT                             1898

void setup () {
    Serial.begin (9600); /* Para a comunicação com o bluetooth */
    SetAllConfigurations ();

    int treshold = 255;
    const uint8_t pin = 13;
    const unsigned short sensorAmount = 2;

    const auto DoLambda = [](){
        return true;
    }
}

void loop () {
    DoStateMachineThingies ( );
}