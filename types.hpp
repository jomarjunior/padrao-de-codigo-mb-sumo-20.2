/* MINERVABOTS
 * A documentação deste código encontra-se na nossa wiki.
 * Wiki: <link-da-wiki>
 * <Descrição do arquivo>
 * Esse arquivo tem como intuito definir um modelo para a programação do novo código dos robôs de sumô da equipe.
 * As lógicas e os métodos de programação apresentados aqui servem apenas de modelos e/ou exemplos sem qualquer
 * relação com a realidade. {Substitui <Descrição do arquivo>}
 */

#ifndef _TYPES_H_
    #define _TYPES_H__

    /* Tipo para identificar os sensores como inteiros */
    typedef enum {
        farLeft,
        left,
        center,
        right,
        farRight,
        sensorAmount
    } sensorType;

    /* Tipo para identificar os motores */
    typedef enum {
        left,
        right
    } motorType;

#endif