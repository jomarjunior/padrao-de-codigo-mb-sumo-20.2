/* MINERVABOTS
 * A documentação do código pode ser encontrada na nossa wiki.
 * Wiki: <link-da-wiki>
 * <Descrição do arquivo>
 * Esse arquivo tem como intuito definir um modelo para a programação do novo código dos robôs de sumô da equipe.
 * As lógicas e os métodos de programação apresentados aqui servem apenas de modelos e/ou exemplos sem qualquer
 * relação com a realidade. {Substitui <Descrição do arquivo>}
 */

#ifndef _SENSORS_HPP_
    #define _SENSORS_HPP_
    #include "types.hpp"

    typedef enum {
        leftIr,
        rightIt,
        sensorAmount
    } sensorType;

    /* Retorna o valor do pino associado ao sensor digital fornecido em 'sensor'.
    * sensorType sensor (E) - Inteiro que representa um sensor digital
    */
    uint8_t GetDigitalSensorPin ( sensorType sensor) {
        return DIGITAL_SENSOR_PINOUT[sensor];
    }

    /* Calcula o PID e devolve o valor em *pid.
    * int erro (E) - Inteiro que corresponde ao erro do sensor.
    * float *pid (S) - Real que corresponde o valor calculado do pid. [0,255] */
    void CalcPID ( int erro, float *pid ){
        *pid = magica;
    }

    /* Retorna o valor de leitura do sensor associado ao inteiro fornecido em 'sensor'
    * sensorType sensor (E) - Inteiro que representa um sensor digital */
    bool
    GetDigitalSensorReadings ( sensorType sensor ) {
        return digitalRead ( GetDigitalSensorPin (sensor) );
    }

#endif